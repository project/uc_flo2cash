(function ($) {
  Drupal.behaviors.uc_flo2cash = {
    attach: function() {
      $('#uc-cart-checkout-review-form #edit-back')
        .removeClass('element-invisible')
        .click(function() {
          parent.history.back();
          return false;
        });
    }
  };
})(jQuery);
