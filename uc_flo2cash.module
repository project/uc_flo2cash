<?php

/**
 * @file
 * Ubercart payment method for Flo2Cash Web2Pay.
 */

/**
 * Implements hook_payment_method().
 */
function uc_flo2cash_uc_payment_method() {
  $methods[] = array(
    'id' => 'uc_flo2cash',
    'name' => t('Flo2Cash Web2Pay'),
    'title' => t('Flo2Cash Web2Pay'),
    'desc' => t('Process credit card payments with flo2cash.'),
    'callback' => 'uc_payment_method_flo2cash_web2pay',
    'weight' => 1,
    'checkout' => FALSE,
    'backend' => FALSE,
  );
  return $methods;
}

/**
 * Implements hook_uc_payment_gateway().
 */
function uc_flo2cash_uc_payment_gateway() {
  $gateways[] = array(
    'id' => 'uc_flo2cash_web2pay',
    'title' => t('Flo2Cash Web2Pay'),
    'description' => t('Process credit card payments with flo2cash.'),
    // 'flo2cash' => 'uc_flo2cash_transaction_notify',
  );
  return $gateways;
}

/**
 * Implements hook_menu().
 */
function uc_flo2cash_menu() {
  $items['cart/flo2cash/process'] = array(
    'title' => 'Process order',
    'page callback' => 'uc_flo2cash_transaction_notify',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['cart/flo2cash/complete'] = array(
    'title' => 'Order complete',
    'page callback' => 'uc_flo2cash_transaction_complete',
    'access callback' => 'user_access',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Adds Flo2Cash settings to the payment method settings form.
 *
 * @see flo2cash_web2pay()
 */
function uc_payment_method_flo2cash_web2pay($op, &$order, $form = NULL, &$form_state = NULL) {
  switch ($op) {
    case 'settings':
      $form['flo2cash']['uc_flo2cash_web2pay_url'] = array(
        '#type' => 'textfield',
        '#title' => t('web2pay URL'),
        '#default_value' => variable_get('uc_flo2cash_web2pay_url',
          'https://secure.flo2cash.co.nz/web2pay/default.aspx'),
      );
      $form['flo2cash']['uc_flo2cash_web2pay_verify_url'] = array(
        '#type' => 'textfield',
        '#title' => t('web2pay verification URL'),
        '#default_value' => variable_get('uc_flo2cash_web2pay_verify_url',
          'https://secure.flo2cash.co.nz/web2pay/MNSHandler.aspx'),
      );
      $form['flo2cash']['uc_flo2cash_account_id'] = array(
        '#type' => 'textfield',
        '#title' => t('flo2cash account ID'),
        '#default_value' => variable_get('uc_flo2cash_account_id', ''),
      );
      return $form;
  }
}

/**
 * Settings form callback.
 */
function uc_flo2cash_settings_form($form, &$form_state) {
  $form['flo2cash']['uc_flo2cash_web2pay_url'] = array(
    '#type' => 'textfield',
    '#title' => t('web2pay URL'),
    '#default_value' => variable_get('uc_flo2cash_web2pay_url',
      'https://secure.flo2cash.co.nz/web2pay/default.aspx'),
  );
  $form['flo2cash']['uc_flo2cash_web2pay_verify_url'] = array(
    '#type' => 'textfield',
    '#title' => t('web2pay verification URL'),
    '#default_value' => variable_get('uc_flo2cash_web2pay_verify_url',
      'https://secure.flo2cash.co.nz/web2pay/MNSHandler.aspx'),
  );
  $form['flo2cash']['uc_flo2cash_account_id'] = array(
    '#type' => 'textfield',
    '#title' => t('flo2cash account ID'),
    '#default_value' => variable_get('uc_flo2cash_account_id', ''),
  );
  return $form;
}

/**
 * Build the POST submission for web2pay.
 */
function uc_flo2cash_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'uc_cart_checkout_review_form') {
    $order_id = intval($_SESSION['cart_order']);
    $order = uc_order_load($order_id);
    if ($order->payment_method == 'uc_flo2cash') {
      $data = array();

      // Add a line item for each product being ordered.
      $i = 0;
      $subtotal = 0;
      foreach ($order->products as $product) {
        // Web2Pay does not accept line items with a price of $0.00.
        if ($product->price == 0) {
          continue;
        }
        $i++;
        $data['item_name' . $i] = $product->title;
        // Web2Pay does not accept line items with a length of >50 chars.
        if (strlen($product->title) > 49) {
          $data['item_name' . $i] = substr($product->title, 0, 46) . '...';
        }
        $data['item_price' . $i] = uc_currency_format($product->price, FALSE, FALSE, '.');
        $data['item_code' . $i] = $product->nid;
        $data['item_qty' . $i] = $product->qty;
        $subtotal += bcadd($subtotal, bcmul($product->price, $product->qty, 2), 2);
      }

      // Add line items for everything which is not the subtotal.
      foreach ($order->line_items as $item) {
        // Check if line item subtotal != products subtotal. If so, then
        // overwrite existing line items with a single entry for subtotal
        // as a line item. Slightly uglier, but more correct.
        if ($item['type'] == 'subtotal') {
          if ($item['amount'] != $subtotal) {
            $i = 1;
            $data = array(
              'item_name1' => t('Subtotal'),
              'item_price1' => uc_currency_format($item['amount'], FALSE, FALSE, '.'),
              'item_code1' => '-',
              'item_qty1' => 1,
            );
          }
        }
        if ($item['type'] != 'subtotal' && $item['amount'] > 0) {
          $i++;
          $data['item_name' . $i] = $item['title'];
          if (strlen($item['title']) > 49) {
            $data['item_name' . $i] = substr($item['title'], 0, 46) . '...';
          }
          $data['item_price' . $i] = uc_currency_format($item['amount'], FALSE, FALSE, '.');
          $data['item_code' . $i] = '';
          // Could use $item['type'] to include detail.
          $data['item_qty' . $i] = "1";
        }
      }

      // Basic settings.
      $data['cmd']              = '_xcart';
      $data['account_id']       = variable_get('uc_flo2cash_account_id', '');
      $data['notification_url'] = url('cart/flo2cash/process', array('absolute' => TRUE));
      $data['return_url']       = url('cart/flo2cash/complete', array('absolute' => TRUE));
      $data['reference']        = $order->order_id;

      // Alter the POST URL and add the web2pay form variables.
      $form['#action'] = variable_get('uc_flo2cash_web2pay_url', 'https://secure.flo2cash.co.nz/web2pay/default.aspx');
      // http://drupal.org/node/821932#comment-6017528
      $form['actions']['submit']['#name'] = '';
      foreach ($data as $name => $value) {
        $form[$name] = array(
          '#type' => 'hidden',
          '#value' => $value
        );
      }
      $form['#after_build'][] = 'uc_flo2cash_checkout_review_form_modify';
    }
  }
}

/**
 * Remove Drupal form additions from external form.
 *
 * @see http://drupal.org/node/821932#comment-6017528
 */
function uc_flo2cash_checkout_review_form_modify($form) {
  unset($form['form_token']);
  unset($form['form_build_id']);
  unset($form['form_id']);
  // Added by uc_credit if present.
  unset($form['sescrd']);
  // Don't show Back button by default. Make it visible if we can make
  // it work via JS.
  if (isset($form['actions']['back'])) {
    $form['actions']['back']['#attributes']['class'][] = 'element-invisible';
    drupal_add_js(drupal_get_path('module', 'uc_flo2cash') . '/uc_flo2cash.js');
  }
  return $form;
}

/**
 * Page callback for Flo2Cash MNS notification URL.
 */
function uc_flo2cash_transaction_notify() {
  module_load_include('inc', 'uc_flo2cash');
  // Verify the submitted data via Flo2Cash MNS.
  if (uc_flo2cash_verify_transaction($_POST)) {
     // The payment info has been validated, so update the Ubercart order.
    $order_id = intval($_POST['reference']);
    if ($order = uc_order_load($order_id)) {
      // Credit card transaction must be successful.  (status code of 2)
      if ($_POST['transaction_status'] != 2) {
        watchdog('uc_flo2cash', 'Payment attempt failed for transaction @txn_id: @rsp_text', array('@txn_id' => $_POST['transaction_id'], '@rsp_text' => $_POST['response_text']), WATCHDOG_WARNING);
        uc_order_comment_save($order_id, 0, t('Unsuccessful payment attempt: !resp', array('!resp' => $_POST['response_text'])), 'admin');
        return;
      }

      $total = 0;
      $i = 1;
      while (isset($_POST['item_price' . $i])) {
        $price = $_POST['item_price' . $i];
        $qty = $_POST['item_qty' . $i];
        $total = bcadd($total, bcmul($price, $qty, 2), 2);
        $i++;
      }
      $comment = t('Transaction ID: @transaction_id', array('@transaction_id' => $_POST['transaction_id']));
      $targs = array(
        '@amount' => uc_currency_format($total),
        '@order_id' => $order_id,
      );

      uc_payment_enter($order_id, 'flo2cash_web2pay', $total, $order->uid, NULL, $comment);
      $output = uc_cart_complete_sale($order);
      uc_order_comment_save($order_id, 0, t('Payment of @amount received via Flo2Cash.', $targs), 'order', 'payment_received');
      uc_order_comment_save($order_id, 0, t('Flo2Cash MNS reported a payment of @amount.', $targs));
      watchdog('uc_flo2cash', 'Flo2Cash payment of @amount verified for order @order_id.', $targs, WATCHDOG_NOTICE, l('View order', 'admin/store/orders/' . $order_id));
      return $output;
    }
    else {
      watchdog('uc_flo2cash', 'Transaction verified, but unable to load order @order_id.', array('@order_id' => $order_id), WATCHDOG_WARNING);
      return;
    }
  }
}

/**
 * Page callback for customer return URL.
 */
function uc_flo2cash_transaction_complete() {
  $order_id = intval($_POST['reference']);
  $order = uc_order_load($order_id);

  // If the transaction was declined, send the user back to their shopping
  // cart.
  if ($_POST['txn_status'] != 2) {
    drupal_set_message(t('The payment was not successful: <strong>@error</strong>.', array('@error' => $_POST['response_text'])), 'error');
    drupal_goto('cart');
  }

  // If the order has been paid for, complete the sale. MNS has already fired
  // this, but we repeat it to obtain the confirmation message.
  if (uc_payment_balance($order) <= 0) {
    $output = uc_cart_complete_sale($order);
    return $output;
  }
  else {
    drupal_set_title(t('Order being processed'));
    watchdog('uc_flo2cash', 'Payment for order #!num was not recorded.', array('!num' => $order_id), WATCHDOG_ERROR);
    return t('We are awaiting confirmation from the payment processor of your order. Please !view_order_status or check your email for updates.', array('!view_order_status' => l('view your order status', 'user/' . $order->uid . '/orders/' . $order->order_id)));
  }
}
